import * as EventSource from 'eventsource';
import { ICommentProvider, IComment } from 'interfaces';
import AccessTokenProvider, { IAccessTokenProvider } from 'providers/AccessTokenProvider';
import { LIVE_VIDEO_ID } from 'config';
import { EventEmitter } from 'events';
import { IProductModel } from 'models/IProductModel';
import productSearch from 'utils/productSearch';

export default class CommentsProvider extends EventEmitter implements ICommentProvider {
	/*private before: string = '';

	getComments = (): Promise<IComment[]> => {
		return new Promise<IComment[]>((resolve, reject) => fetch('https://graph.facebook.com/')
			.then(res => resolve([]))//{from: 'Ivan Sivak', text: 'Hello, world!'}]))
			.catch(err => reject(err)));
	}*/

	private readonly _ATProvider: IAccessTokenProvider;
	private _stream: EventSource;

	constructor() {
		super();
		this._ATProvider = new AccessTokenProvider();
	}

	private initStream() {
		const token = this._ATProvider.getToken();

		this._stream = new EventSource(`https://streaming-graph.facebook.com/${LIVE_VIDEO_ID}/live_comments?access_token=${token}&comment_rate=ten_per_second&fields=from{name,id},message`);
		this._stream.addEventListener('error', (err: object) => this.emit('error', err));
		this._stream.addEventListener('open', () => this.emit('open'));
		this._stream.addEventListener('message', (m: { [key: string]: string }) => {
			const comment = JSON.parse(m.data) as IComment;
			const item = productSearch(comment.message);
			this.emit('comment', comment, item);
		});
	}

	emit(event: 'comment' | 'error' | 'open', ...args: any[]) {
		super.emit(event, ...args);
		return true;
	}

	initConnection() {
		this.initStream();
	}
}