import * as path from 'path';
import * as SQLite from 'sqlite3';
import { DB_FILENAME } from 'config';

const db = new SQLite.Database(path.join(__dirname, '../../db/' + DB_FILENAME), SQLite.OPEN_CREATE | SQLite.OPEN_READWRITE, err => {
	if(err) {
		return console.error('An error has occured while trying to connect to the database.', err);
	}

	console.log('Connection to the database has been established.');
});

export default db;