import { ACCESS_TOKENS } from "config";

export interface IAccessTokenProvider {
	getToken(): string;
}

export default class AccessTokenProvider implements IAccessTokenProvider {
	private _token: number = 0;

	getToken() {
		if(this._token === ACCESS_TOKENS.length) {
			this._token = 0;
		}
		
		return ACCESS_TOKENS[this._token++];
	}
}