import * as http from 'http';
import * as express from 'express';
import fetch from 'node-fetch';
import CommentsProvider from 'providers/CommentsProvider';
import { Socket } from 'net';
import { setInterval, setTimeout } from 'timers';
import { IComment, ICommentAPIResponse } from 'interfaces';

import App from 'App';
import WebSocket from 'websocket';
import CommentsRepository from 'infrastructure/commentsRepository';

const PORT = Number(process.env.PORT) || 3000;
App.set('port', PORT);

const connectionHandler = (socket: Socket): void => {
	console.log(socket);
}

const listeningHandler = (): void => {
	console.log(`App listening on: http://localhost:${PORT}`);
}

const server: http.Server = http.createServer(App);
server.listen(PORT, '0.0.0.0');
server.on('listening', listeningHandler);

// Connect to FB
const commentsProvider = new CommentsProvider();
commentsProvider.on('open', () => console.log('Connection to Facebook LIVE comments stream has been established.'));
commentsProvider.on('error', (err) => console.error('An error with Facebook LIVE comments connection has occured.', err));
commentsProvider.initConnection();

// Comments with DB
const commentsRepository = new CommentsRepository(commentsProvider);

// WebSocket
const wss = WebSocket(commentsProvider, commentsRepository);

// Close DB Connection on process end
process.on('SIGINT', () => {
	commentsRepository.closeConnection();
});