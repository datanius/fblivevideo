import { IProductModel } from "models/IProductModel";
import { allProducts } from '../infrastructure/productsRepository';

const lookForProduct = (productPhrase: string): IProductModel => {
	let found: IProductModel = null;
	productPhrase = productPhrase.trim().toLocaleLowerCase();
	allProducts().forEach((p, idx) => {
		p.phrases.forEach(phrase => {
			if(productPhrase.indexOf(phrase.toLocaleLowerCase()) >= 0) {
				found = {
					idx: idx,
					name: p.name,
					phrases: p.phrases
				};

				return;
			}
		});
	});

	return found;
}

export default lookForProduct;