import * as WebSocket from 'ws';
import fetch from 'node-fetch';
import { IComment, ICommentAPIResponse } from 'interfaces';

const wss = new WebSocket.Server({
	port: 3001
});

wss.on('connection', (ws, req) => {

});

const VIDEO_ID = '2033680623310990';
const ACCESS_TOKEN = 'EAAJWMATu4BoBAA8BbDz9nJNmB5beh5AmxabuGZAFq1owiNdNYGHAQNOu9pxtDZCECEWaGHduFbrGnOfVfcr0GoiE5WZAZBDIsjdvyvjwZAbBdvxPMs15LeoJSN4ucu6KhNZANgU4DiLJ4OwRVbsJ7bIiPZB2U903n24NgTfEZBsC3YeIKzVk5EUkSurXZAcYMxknQQwQMCve8MbWo4yDSuMez';
let before: string = '';

const pollData = () => {
	setInterval(() => {
		fetch(`https://graph.facebook.com/v3.1/${VIDEO_ID}/comments?access_token=${ACCESS_TOKEN}&filter=stream&live_filter=no_filter&order=reverse_chronological${before !== '' ? '&before=' + before: ''}`)
			.then(res =>
				res.json()
					.then((resJson: ICommentAPIResponse) => {
						wss.clients.forEach(c => {
							c.send(JSON.stringify({
								type: 'PING'
							}));
						});

						const comments = resJson.data;
						if(comments !== undefined && comments.length > 0) {
							before = resJson.paging.cursors.before;
							wss.clients.forEach(c => c.send(JSON.stringify({
								type: 'COMMENTS_RECEIVED',
								payload: comments
							})));
						}
					})
					.catch(err => console.error(err)))
			.catch(err => console.error(err));
	}, 5000);

	// 600 calls per 600 seconds
	// https://developers.facebook.com/docs/videos/live-video/production-broadcasts#comments
	// https://developers.facebook.com/docs/graph-api/advanced/rate-limiting
};

export default pollData;