export interface IProductModel {
	idx?: number;
	name: string;
	phrases: string[];
}