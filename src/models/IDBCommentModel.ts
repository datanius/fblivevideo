export interface IDBCommentModel {
	name: string;
	comment: string;
	item: number;
	date: string;
}