export interface IUserEntryModel {
	name: string;
	product: string;
	productId: number;
}