import * as WebSocket from 'ws';
import CommentsProvider from 'providers/CommentsProvider';
import CommentsRepository from 'infrastructure/commentsRepository';
import { IUserEntryModel } from 'models/IUserEntryModel';
import { IComment } from 'interfaces';
import { IProductModel } from 'models/IProductModel';

const wss = new WebSocket.Server({
	port: 3001
});

const emitToClients = (commentsProvider: CommentsProvider, commentsRepository: CommentsRepository) => {
	commentsProvider.on('comment', (comment: IComment, product: IProductModel) => {
		if(product === null) {
			return;
		}

		/*commentsRepository.getCommentByProductId(product.idx, (res) => {
			if(res) {
				return;
			}*/
		
			const payload: IUserEntryModel = {
				name: comment.from.name,
				product: product.name,
				productId: product.idx
			};

			wss.clients.forEach(c => c.send(JSON.stringify({
				type: 'ENTRY_RECEIVED',
				payload: payload
			})));

			console.log(payload);
		//});
	})};

wss.on('listening', () => console.log(`WebSocket connection established on port ${wss.options.port}`));
export default emitToClients;