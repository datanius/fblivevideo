export interface IAPICursors {
	before: string;
	after: string;
}

export interface IAPIPaging {
	cursors: IAPICursors;
}

export interface ICommentAPIResponse {
	data: IComment[];
	paging: IAPIPaging;
	error?: object;
}

export interface IUser {
	id: string;
	name: string;
}

export interface IComment {
	view_id: number;
	created_time?: string;
	from: IUser;
	message: string;
	id: string;
}

export interface ICommentProvider {
	// getComments(): Promise<IComment[]>;

	initConnection(): void;
	// emit(): void;
}