import { Router, Request, Response, NextFunction } from 'express';
// api methods
import comments from './controllers/Comments';


class Api {
	public router: Router;

	apiWelcome(req: Request, res: Response) {
		res.send('<h1>It works</h1>');
	}
	
	private verify(req: Request, res: Response, next: NextFunction) {
		const token = req.body.token || req.query.token || req.headers['x-access-token'];
		next();
	}

	constructor() {
		this.router = Router();

		// this.router.use(this.verify);

		this.router.get('/', this.apiWelcome);
		this.router.use('/comments', comments);
	}
}

export default new Api().router;