// import fetch from 'node-fetch';
import { Router, Request, Response, NextFunction } from 'express';
import CommentsProvider from 'providers/CommentsProvider'

class Comments {
	router: Router;

	private verify(req: Request, res: Response) {
		console.log(req.query);
		res.send(req.query['hub.challenge']);
		//35D1B7E4B05ED818FFF9A113CB57DF4
	}

	private receive(req: Request, res: Response) {
		console.log(req.body.entry[0].changes);
		res.send('ok');
	}

	private getComments(req: Request, res: Response) {
		res.send('ok');
		/*new CommentsProvider().getComments()
			.then(comments => res.json(comments))
			.catch(err => res.status(400).json(err));*/
	}

	constructor() {
		this.router = Router();
		this.router.get('/', this.verify);
		this.router.post('/', this.receive);
		this.router.post('/get', this.getComments);
	}
}

export default new Comments().router;