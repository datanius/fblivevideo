import { IProductModel } from "models/IProductModel";

const products: IProductModel[] = require('../../products.json');

const allProducts = () => products;

export { allProducts };