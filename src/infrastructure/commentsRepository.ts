import db from 'providers/DatabaseProvider';
import CommentsProvider from 'providers/CommentsProvider';
import { IComment } from 'interfaces';
import { IDBCommentModel } from 'models/IDBCommentModel';
import { DB_COMMENTS_NAME } from 'config';
import { IProductModel } from 'models/IProductModel';

export default class CommentsRepository {
	private readonly commentsProvider: CommentsProvider;

	constructor(commentsProvider: CommentsProvider) {
		this.commentsProvider = commentsProvider;
		this.initTable();
		this.createBinding();
	}

	initTable() {
		db.run(`CREATE TABLE if not exists ${DB_COMMENTS_NAME} (id INTEGER PRIMARY KEY, name VARCHAR(220), comment TEXT, item INT, date DATETIME)`);
	}

	insertComment(comment: IComment, item: IProductModel) {
		db.run(`INSERT INTO ${DB_COMMENTS_NAME} (name, comment, item, date) VALUES(?, ?, ?, DATETIME('now', 'localtime'))`, [comment.from.name, comment.message, item.idx]);
	}

	getComments() {
		db.all(`SELECT * FROM ${DB_COMMENTS_NAME} ORDER BY id DESC`, (err, rows: IDBCommentModel[]) => {
			rows.forEach(row => console.log(row));
		});
	}

	getCommentByProductId(productId: number, callback: (result: boolean) => void) {
		db.all(`SELECT * FROM ${DB_COMMENTS_NAME} WHERE item = ${productId}`, (err, rows) => {
			callback(rows !== null && rows.length > 0);
		});
	}

	createBinding() {
		this.commentsProvider.on('comment', (comment: IComment, product: IProductModel) => {
			if(product === null) {
				return;
			}

			this.insertComment(comment, product);
		});
	}

	closeConnection() {
		db.close();
	}
}