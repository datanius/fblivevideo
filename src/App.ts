import * as express from 'express';
import * as bodyParser from 'body-parser';
import Api from './api';

const JSONBodyParser = bodyParser.json();

class App {
	public express: express.Application;

	constructor() {
		this.express = express();
		this.express.use('/api', JSONBodyParser, Api);
	}
}

export default new App().express;