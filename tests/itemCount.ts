import { IProductModel } from '../src/models/IProductModel';
import { it, describe } from 'mocha';
import { expect } from 'chai';
import { allProducts } from '../src/infrastructure/productsRepository';

describe('All products', () => {
	it('should have 50 items in total', () => {
		expect(allProducts().length).to.equal(50)
	});
});