import { IProductModel } from '../src/models/IProductModel';
import { it, describe } from 'mocha';
import { expect } from 'chai';
import productSearch from '../src/utils/productSearch';

describe('Found product', () => {
	it('should have found zmizik in products', () => {
		const product = productSearch('Zmizik');
		expect(product.idx).to.equal(productSearch('zmizík').idx)
	});
});